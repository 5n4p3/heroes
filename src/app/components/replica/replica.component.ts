import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from '../../services/heroes.service';

@Component({
  selector: 'app-replica',
  templateUrl: './replica.component.html',
  styleUrls: ['./replica.component.css']
})
export class ReplicaComponent implements OnInit {

  heroes: Heroe[];

  constructor(
    private heroeService: HeroesService
  ) { }

  ngOnInit(): void {
    this.heroes = this.heroeService.getHeroes();
  }

}
