import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  mensaje: any = {
    titulo: 'Heroes App',
    subtitulo: 'Información de SuperHeroes'
  };

  constructor() {
   }

  ngOnInit(): void {
    this.buscar('new Date()', 'texto', '17/09/1994');
  }

  buscar(texto: string, edad: string, fecha: string) {
  }

}
