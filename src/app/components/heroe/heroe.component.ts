import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService, Heroe } from '../../services/heroes.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent {

  heroe: Heroe;
  origenComponente: string;

  constructor(private activatedRoute: ActivatedRoute,
              private heroesService: HeroesService
    ){
      // La propiedad params es un Observable, por ende se debe subscribir a él
    this.activatedRoute.params.subscribe(params => {
      this.heroe = this.heroesService.getHeroe( params.id );
      this.origenComponente = `/${params.origen}`;
    });
   }

   verHeroe(): void {
     console.log('Accedio a la función');
   }
}
