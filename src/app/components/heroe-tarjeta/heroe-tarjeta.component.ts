import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Heroe } from '../../services/heroes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroe-tarjeta',
  templateUrl: './heroe-tarjeta.component.html',
  styleUrls: ['./heroe-tarjeta.component.css']
})
export class HeroeTarjetaComponent implements OnInit {

  @Input() heroe: Heroe; // Los INPUT son utilizados para hacer la comunicacion de un elemento Padre a un elemento hijo,
                            // Este mismo atributo "heroe" puede ser usado sin problemas en este mismo componente, el docorador
                            // solo esta indicando que puede o no venir un valor enviado desde el elemento padre.
                            // Si inicializo el elemento y RECIBO valores del padre se trabaja con los valores recibidos
                            // Si inicializo el elemento y NO RECIBO valores del padre entonces trabajara con el valor iniciliazado
  @Input() index: number;
  @Output() heroeSeleccionado: EventEmitter<number>;
  // El Output le va a indicar al elemento padre que debe estar atento a las acciones realizados en "heroeSeleccionado" del hijo,
  // cuando este sea gatillado el padre sabe que debe realizar la accion. El "Output" va de la mano con el "EventEmitter", ya que este
  // es el encargado de reconocer que se produjo la accion. Al "EventEmmiter" se le debe definir el tipo de dato que retorna.
  // Basicamente al definir este Output estoy diciendo que "heroeSeleccionado" es un Observable y que
  // EventEmitter enviara un evento personalizado de este elemento

  constructor(private router: Router) {
    this.heroeSeleccionado = new EventEmitter(); // Se debe inicializar, de lo contrario no va a funcionar
   }

  ngOnInit(): void {
  }

  verHeroe2(){
    this.heroeSeleccionado.emit( this.index ); // Se va emitir el valor del "index" al elemento padre
  }

  verHeroe() {
    this.router.navigate(['/heroe', this.index]); // El navigate en un arreglo exactamente igual al routerLink
  }

}
