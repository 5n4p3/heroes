import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { HeroesComponent } from './components/heroes/heroes.component';
import { HeroeComponent } from './components/heroe/heroe.component';
import { SearchComponent } from './components/search/search.component';
import { Prueba2Component } from './components/prueba2/prueba2.component';
import { ReplicaComponent } from './components/replica/replica.component';


const APP_ROUTES: Routes = [
   {path: 'home', component: HomeComponent},
   {path: 'about', component: AboutComponent},
   {path: 'heroes', component: HeroesComponent},
   {path: 'prueba2', component: Prueba2Component},
   {path: 'replica', component: ReplicaComponent},
   {path: 'heroe/:id/:origen', component: HeroeComponent},
   {path: 'search/:texto', component: SearchComponent},
   {path: '**', pathMatch: 'full', redirectTo: 'home'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
// La opcion "{useHash: true}" es lo que va a mostrar "#/" en la URL
// Segun la configuración del servidor donde vaya a desplegar se puede usar o no
// Como regla general, si tengo control del servidor esta puedo dejarla desactivada, que simplemente seria remover ese objeto
// Si NO tengo control del servidor o no conozco su configuración se deja activa esta opción por seguridad.
// Si el servidor no esta correctamente configurado y no tengo esta opcion activa es muy probable tener problemas con las rutas.
// Lo mas grave que puede ocurrir es perder parametros que se envien por URL o apuntar a paths y que no los reconozca el servidor
